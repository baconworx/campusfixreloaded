﻿using System.Text.RegularExpressions;

namespace campusfixreloaded.DataClasses
{
    public class ReplacementPattern
    {
        public Regex Regex { get; set; }
        public string Replacement { get; set; }

        public ReplacementPattern(Regex regex, string replacement)
        {
            Regex = regex;
            Replacement = replacement;
        }
    }
}
