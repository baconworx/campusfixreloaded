﻿namespace campusfixreloaded.Models
{
    public class ComputedLinkResult
    {
        public bool Ok { get; set; }
        public string ComputedLink { get; set; }

        public ComputedLinkResult(bool ok, string computedLink)
        {
            Ok = ok; /* rammus approves! */
            ComputedLink = computedLink;
        }
    }
}
