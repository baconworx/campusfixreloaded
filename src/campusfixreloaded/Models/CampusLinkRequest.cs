﻿namespace campusfixreloaded.Models
{
    public class CampusLinkRequest
    {
        public string CampusLink { get; set; }
        public bool LectureTypePrefix { get; set; }
    }
}
