﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using campusfixreloaded.DataClasses;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Net.Http.Headers;

namespace campusfixreloaded.Controllers
{
    public class ApiController : Controller
    {
        private const string MED_CAMPUS_CALENDAR_URI = "http://campus.meduniwien.ac.at/med.campusj/ws/termin/ical?pStud={0}&pToken={1}";

        internal static readonly ReplacementPattern[] Patterns = 
        {
            new ReplacementPattern(new Regex(@"DESCRIPTION:fix\\; Abhaltung\\; ", RegexOptions.Compiled), "DESCRIPTION:"),
            new ReplacementPattern(new Regex(@"SUMMARY:(.*?)\r\n?\s?DESCRIPTION:(.*?)\\?\;\s?(.*?)\\?\;\s?\r\n?\s*?DTSTART", RegexOptions.Singleline | RegexOptions.Compiled),
                                   "SUMMARY:$2\nDESCRIPTION:$1 $3\nDTSTART"),
            new ReplacementPattern(new Regex(@"SUMMARY:(.*?)\nDESCRIPTION:(.*?)(VO|SK|SE|KP|PR|UE|FP)\\,\s?", RegexOptions.Singleline | RegexOptions.Compiled),
                                   "SUMMARY:($3) $1\nDESCRIPTION:$2")

        };

        internal const int LECTURE_TYPE_PREFIX_REGEX_INDEX = 2;

        [HttpGet]
        public IActionResult GetIcal(string student, string token, bool lectureTypePrefix)
        {
            var client = new HttpClient();
            var requestTask = client.GetStringAsync(string.Format(MED_CAMPUS_CALENDAR_URI, student, token));

            string response = null;
            try
            {
                requestTask.Wait(10000);
                response = requestTask.Result;
            }
            catch { /* ignored */ }

            var result = new ContentResult();

            if (response != null)
            {
                result.ContentType = new MediaTypeHeaderValue("text/calendar");
                result.Content = prettifyMedCalendar(response, lectureTypePrefix);
            }
            else
            {
                result.ContentType = new MediaTypeHeaderValue("text/html");
                result.Content = new TagBuilder("p") { InnerHtml = "error while getting ical from medcampus server." }.ToHtmlString(TagRenderMode.Normal).ToString();
                result.StatusCode = 500;
            }


            return result;
        }

        private string prettifyMedCalendar(string icalString, bool lectureTypePrefix)
        {
            var processPatterns = lectureTypePrefix
                ? Patterns
                : Patterns.Except(new [] { Patterns[LECTURE_TYPE_PREFIX_REGEX_INDEX] }).ToArray();

            var events = icalString.Split(new[] { "END:VEVENT" }, StringSplitOptions.None);
            for (var i = 0; i < events.Length; i++)
                events[i] = processPatterns.Aggregate(events[i], (current, pattern) => pattern.Regex.Replace(current, pattern.Replacement));

            return events.Aggregate((current, next) => current + "END:VEVENT" + next);
        }
    }
}
