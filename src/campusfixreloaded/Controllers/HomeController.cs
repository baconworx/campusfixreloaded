﻿using System;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using campusfixreloaded.Extensions;
using campusfixreloaded.Models;
using Microsoft.AspNet.Http.Extensions;
using Microsoft.AspNet.Mvc;

namespace campusfixreloaded.Controllers
{
    public class HomeController : Controller
    {
        private readonly Regex _medCampusUrlPattern = new Regex(@"^https://campus\.meduniwien\.ac\.at/med\.campusj/ws/termin/ical\?pStud=(?<student>[A-F0-9]+)&pToken=(?<token>[A-F0-9]+)$");

        public IActionResult Index()
        {
            ViewBag.ReplacementPatterns = ApiController.Patterns;
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ComputeLink([FromBody]CampusLinkRequest campuslink)
        {
            var match = _medCampusUrlPattern.Match(campuslink.CampusLink);
            if (match.Success)
                return Json(new ComputedLinkResult(true, "http://{url}/{student}/{token}?lectureTypePrefix={prefix}&nocache={nocache}".FormatWith(new
                {
                    url = Request.Host.Value,
                    student = match.Groups["student"],
                    token = match.Groups["token"],
                    prefix = campuslink.LectureTypePrefix.ToString(),
                    nocache = DateTime.Now.Millisecond * DateTime.Now.Second // add a random number so google reloads it
                })));

            return Json(new ComputedLinkResult(false, null));
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            ViewBag.BasePath = UriHelper.Encode(Request.Scheme, Request.Host);
            ViewBag.AbsolutePath = UriHelper.Encode(Request.Scheme, Request.Host, Request.PathBase, Request.Path, Request.QueryString);
        }
    }
}
